﻿using LazyLoading.Contracts;
using LazyLoading.Data;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web.Http;
using LazyLoading.Models;

namespace LazyLoading.Controllers
{
    public class MainController : ApiController
    {
        private AppDBContext db = new AppDBContext();

        [HttpPost]
        [Route(MainContract.Rides.Route)]
        public string Rides(MainContract.Rides.Request request)
        {
            //execution audit
            LazyLoadingAudit lazyLoading = new LazyLoadingAudit();

            lazyLoading.Identification = request.Id;
            lazyLoading.executionDate = DateTime.Now;
            db.Audit.Add(lazyLoading);
            db.SaveChanges();

            var sb = new StringBuilder();
            var itemsPerDay = GetItemsByDay(request.Base64);

            if (itemsPerDay.Count > 0)
            {
                for (int i = 0; i < itemsPerDay.Count; i++)
                {
                    var items = itemsPerDay[i]
                        .Where(item => item >= 1 && item <= 100) // 1 ≤ Wi ≤ 100 Restriction
                        .ToList();

                    if (!(items.Count >= 1 && items.Count <= 100)) continue; // 1 ≤ N ≤ 100 Restriction

                    var rides = new List<List<int>>();

                    while (items.Count > 0)
                    {
                        var ride = new List<int>();
                        int max = items.Max();

                        ride.Add(max);
                        items.RemoveAt(items.IndexOf(max));

                        while ((max * ride.Count) < 50 && items.Count > 0)
                        {
                            int min = items.Min();
                            ride.Add(min);
                            items.RemoveAt(items.IndexOf(min));
                        }

                        rides.Add(ride);
                    }

                    sb.AppendLine($"Case {i + 1}: {rides.Count}");

                    if (request.Type == MainContract.Rides.LazyLoadingType.Detail)
                    {
                        string temp = "";
                        foreach (var ride in rides)
                            temp += "(" + string.Join(",", ride) + ") ";

                        sb.AppendLine(temp);
                        sb.AppendLine();
                    }
                }
            }

            string path = Path.GetTempPath() + Guid.NewGuid().ToString() + ".txt";
            File.WriteAllText(path, sb.ToString());
            var bytes = File.ReadAllBytes(path);

            return Convert.ToBase64String(bytes);
        }

        private static List<List<int>> GetItemsByDay(string base64)
        {
            string path = Path.GetTempPath() + Guid.NewGuid().ToString() + ".txt";
            var bytes = Convert.FromBase64String(base64.Split(',')[1]);
            File.WriteAllBytes(path, bytes);

            var inputFile = Array.ConvertAll(File.ReadAllLines(path), int.Parse);
            int t = inputFile.FirstOrDefault(); // Days Wilson Works
            int n = 0; // Amount of items
            var itemsPerDay = new List<List<int>>();

            if (t >= 1 && t <= 500) //1 ≤ T ≤ 500 restriction
            {
                for (int i = 1; i < inputFile.Length; i++)
                {
                    n = inputFile[i];
                    var itemsWi = new List<int>();

                    for (int p = 0; p < n; p++)
                    {
                        i++;
                        itemsWi.Add(inputFile[i]); //Wi Element Weight
                    }
                    itemsPerDay.Add(itemsWi);
                }
            }
            return itemsPerDay;
        }
    }
}
