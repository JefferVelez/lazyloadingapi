﻿namespace LazyLoading.Contracts
{
    public class MainContract
    {
        private const string Base = "api/main";

        public static class Rides
        {
            public const string Route = Base + "/rides";

            public enum LazyLoadingType
            {
                Normal = 1,
                Detail = 2
            }

            public class Request
            {
                public string Id { get; set; }
                public string Base64 { get; set; }
                public LazyLoadingType Type { get; set; }
            }
        }
    }
}