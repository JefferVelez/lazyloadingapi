﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using LazyLoading.Models;

namespace LazyLoading.Data
{
    public class AppDBContext : DbContext
    {
        public AppDBContext() : base("DefaultConnection")
        {

        }
        public DbSet<LazyLoadingAudit> Audit { get; set; }
    }
}