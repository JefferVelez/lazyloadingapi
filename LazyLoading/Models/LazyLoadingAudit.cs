﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LazyLoading.Models
{
    public class LazyLoadingAudit
    {
        [Key]
        public int Id { get; set; }
        public string Identification { get; set; }
        public DateTime executionDate { get; set; }
    }
}